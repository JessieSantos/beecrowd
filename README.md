# beecrowd
Resolução de problemas da plataforma [**beecrowd**](https://www.beecrowd.com.br)
 (antigo *URI Online Judge*).

 ## Sobre o beecrowd
O beecrowd é um sistema online (online judge) que testa se um determinado programa (solução proposta) resolve um dado problema.

O sistema executa a solução proposta pelo usuário, além de testá-la com um conjunto de casos de teste, onde a saída do sistema é comparada aos resultados esperados. A solução submetida deve possuir o mesmo comportamento de entrada/saída especificado no problema.

## Desenvolvedora
Jessica Santos [(jeehgasai@gmail.com)](mailto:jeehgasai@gmail.com)
